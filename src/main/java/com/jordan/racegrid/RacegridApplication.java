package com.jordan.racegrid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RacegridApplication {

	public static void main(String[] args) {
		SpringApplication.run(RacegridApplication.class, args);
	}
}
