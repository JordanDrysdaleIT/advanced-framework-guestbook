package com.jordan.racegrid.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;


@Entity
@Table(name = "entries")
public class racegridEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "entry_id")
    private Integer id;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @NotEmpty
    @Column (name = "driver")
    private String driver;

    @NotEmpty
    @Column (name = "position")
    private String position;

    public String getTyre() {
        return tyre;
    }

    public void setTyre(String tyre) {
        this.tyre = tyre;
    }

    @NotEmpty
    @Column (name = "tyre")
    private String tyre;

    @NotEmpty
    @Column (name = "date")
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
