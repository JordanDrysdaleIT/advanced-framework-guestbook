package com.jordan.racegrid.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface racegridEntryRepository
    extends CrudRepository<racegridEntry, Integer> {

        @Override
        List<racegridEntry> findAll ();

    racegridEntry findRacegridEntryById (Integer id);

    List<racegridEntry> findRacegridEntryByDriver (String driver);
    }

