package com.jordan.racegrid.controller;

import com.jordan.racegrid.domain.racegridEntry;
import com.jordan.racegrid.service.RacegridService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RacegridWebController {

    private static final String RACEGRID_TEMPLATE = "racegrid";

    private static final String ENTRIES_TEMPLATE_ID = "entries";

    private static final String HOMEPAGE_REDIRECT = "redirect:/";

    private static final String NEW_ENTRY_TEMPLATE_ID = "newEntry";

    private static final String RACEGRID_FORM_HEADER_ID = "formHeader";

    @GetMapping ("/")
    public String displayRacegrid (Model model) {

        model.addAttribute(RACEGRID_FORM_HEADER_ID, "Insert a Driver and Their Position!");
        model.addAttribute (ENTRIES_TEMPLATE_ID, this.racegridService.findAllEntries());
        model.addAttribute ("newEntry", new racegridEntry());
        model.addAttribute (NEW_ENTRY_TEMPLATE_ID, new racegridEntry());

        return RACEGRID_TEMPLATE;
    }

    @GetMapping ("/delete/{id}")
    public String deletePosition (@PathVariable Integer id) {
        this.racegridService.deleteRacegridEntryById (id);

        return HOMEPAGE_REDIRECT;
    }

    @GetMapping ("update/{id}")
    public String editPosition (Model model, @PathVariable Integer id) {
        model.addAttribute (ENTRIES_TEMPLATE_ID, this.racegridService.findAllEntries ());
        model.addAttribute (RACEGRID_FORM_HEADER_ID, "Edit Driver Details & Position");
        model.addAttribute (NEW_ENTRY_TEMPLATE_ID, this.racegridService.findOne (id));
        return RACEGRID_TEMPLATE;
    }
    @PostMapping ("update/{id}")
    public String savePosition (Model model,
                               @PathVariable Integer id,
                               @Valid @ModelAttribute (NEW_ENTRY_TEMPLATE_ID)
                                       racegridEntry newEntry, BindingResult bindingResult) {

        //Validate if errors exist on update
        if (!bindingResult.hasErrors()) {
            racegridEntry current = this.racegridService.findOne(id);

            current.setDriver(newEntry.getDriver());
            current.setPosition(newEntry.getPosition());
            current.setTyre(newEntry.getTyre());
            current.setDate(newEntry.getDate());

            this.racegridService.save(current);
            return HOMEPAGE_REDIRECT;
        } else {
            model.addAttribute(RACEGRID_FORM_HEADER_ID, "Edit driver and pos information");
            model.addAttribute(ENTRIES_TEMPLATE_ID, this.racegridService.findAllEntries());

            return RACEGRID_TEMPLATE;
        }
    }

    @PostMapping ("/")
    public String addPosition (Model model,
                              @Valid @ModelAttribute (NEW_ENTRY_TEMPLATE_ID)
                                      racegridEntry newEntry,
                              BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            this.racegridService.save(newEntry);
            return HOMEPAGE_REDIRECT;
        } else {
            model.addAttribute (RACEGRID_FORM_HEADER_ID, "Edit driver and pos information");
            model.addAttribute(ENTRIES_TEMPLATE_ID, this.racegridService.findAllEntries());
            return RACEGRID_TEMPLATE;
        }
    }

    @Autowired
    private RacegridService racegridService;
}
