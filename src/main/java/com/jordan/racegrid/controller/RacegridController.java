package com.jordan.racegrid.controller;

import com.jordan.racegrid.domain.racegridEntry;
import com.jordan.racegrid.service.RacegridService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping ("/api")
@RestController
public class RacegridController {

    @Autowired
    private RacegridService racegridService;

    @GetMapping ("/position")
    public List <racegridEntry> testMapping () {
        return racegridService.findAllEntries ();
    }

    @GetMapping("/position/{id}")
    public racegridEntry findRacegridEntryById (@PathVariable("id") Integer id) {
        return this.racegridService.findRacegridEntryById (id);
    }
    @GetMapping ("/driver/{driver}")
    public List <racegridEntry> findRacegridEntryByDriver (@PathVariable ("driver") String driver) {
        return this.racegridService.findRacegridEntryByDriver (driver);
    }
    @DeleteMapping("/position/{id}")
    public void deleteRacegridEntryById (@PathVariable ("id") Integer id) {
        this.racegridService.deleteRacegridEntryById (id);
    }
    @PostMapping("/add")
    public void addComment (@RequestBody racegridEntry racegridEntry) {
        this.racegridService.save (racegridEntry);
    }
    @PostMapping ("/update")
    public void updateComment (@RequestBody racegridEntry racegridEntry) {
        this.racegridService.save (racegridEntry);
    }
}
