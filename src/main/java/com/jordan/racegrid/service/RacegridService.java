package com.jordan.racegrid.service;

import com.jordan.racegrid.domain.racegridEntry;
import com.jordan.racegrid.domain.racegridEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RacegridService {

    @Autowired
    private racegridEntryRepository racegridEntryRepository;
    public List<racegridEntry> findAllEntries () {

        return this.racegridEntryRepository.findAll ();
    }
    public racegridEntry findRacegridEntryById (Integer id) {

        return this.racegridEntryRepository.findRacegridEntryById(id);
    }
    public List <racegridEntry> findRacegridEntryByDriver (String driver) {
        return this.racegridEntryRepository.findRacegridEntryByDriver (driver);
    }
    public void deleteRacegridEntryById (Integer id) {
        this.racegridEntryRepository.delete (id);
    }
    public void save (racegridEntry newEntry) {
        this.racegridEntryRepository.save (newEntry);
    }
    public racegridEntry findOne (Integer id) {
        return this.racegridEntryRepository.findOne (id);
    }
}
